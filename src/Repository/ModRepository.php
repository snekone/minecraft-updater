<?php

namespace App\Repository;

use App\Entity\Mod;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<Mod>
 *
 * @method Mod|null find($id, $lockMode = null, $lockVersion = null)
 * @method Mod|null findOneBy(array $criteria, array $orderBy = null)
 * @method Mod[]    findAll()
 * @method Mod[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ModRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Mod::class);
    }

    public function findBySlug(string $slug): ?Mod
    {
        return $this->findOneBy(['slug' => $slug]);
    }

    public function findByFilename(string $filename): ?Mod
    {
        return $this->findOneBy(['filename' => $filename]);
    }
}
