<?php

namespace App\Repository;

interface DirectoryRepositoryInterface
{
    public function getDirectoryContents(string $path): array;
}