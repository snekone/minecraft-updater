<?php

namespace App\Repository;

class DirectoryRepository implements DirectoryRepositoryInterface
{

    public function getDirectoryContents(string $directoryPath): array
    {
        if (!is_dir($directoryPath)) {
            throw new \InvalidArgumentException('Directory not found');
        }
        $contents = scandir($directoryPath);
        return array_diff($contents, ['.', '..']);
    }
}