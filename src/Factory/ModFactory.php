<?php

namespace App\Factory;

use App\Entity\Mod;

class ModFactory
{
    public static function create(string $slug, string $filename, string $localVersion, ?string $remoteVersion): Mod
    {
        $mod = new Mod();
        $mod->setSlug($slug);
        $mod->setFilename($filename);
        $mod->setLocalVersion($localVersion);
        $mod->setRemoteVersion($remoteVersion);
        return $mod;
    }
}