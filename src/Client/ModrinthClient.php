<?php

namespace App\Client;

use Symfony\Contracts\HttpClient\HttpClientInterface;

class ModrinthClient implements ModClientInterface
{

    public function __construct(
        private readonly HttpClientInterface $httpClient,
        private string $baseUrl,
    ) {
    }

    public function getMod(string $slug): array
    {
    }

    public function getVersions(string $slug): array
    {
        $url = $this->baseUrl . '/project/' . $slug . '/version?game_versions=["1.20.1"]&loaders=["fabric"]';
        try {
            return $this->httpClient->request('GET', $url)->toArray();
        } catch (\Throwable $e) {
            return [];
        }
    }

    public static function getModUrl(string $slug): string
    {
        return 'https://modrinth.com/mod/' . $slug;
    }
}