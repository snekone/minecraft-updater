<?php

namespace App\Client;

interface ModClientInterface
{
    public function getMod(string $slug): array;

    public function getVersions(string $slug): array;
}