<?php

namespace App\Command;

use App\Repository\ModRepository;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\TableCell;
use Symfony\Component\Console\Helper\TableCellStyle;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

#[AsCommand(
    name: 'minecraft:mods:list',
    description: 'Add a short description for your command',
)]
class MinecraftModsListCommand extends Command
{


    public function __construct(
        private readonly ModRepository $modRepository,
    ) {
        parent::__construct();
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        $mods = $this->modRepository->findAll();
        $table = $io->createTable();
        $table->setHeaders(['Slug', 'Filename', 'Local Version', 'Remote Version']);
        foreach ($mods as $mod) {
            $versionColor = $mod->getLocalVersion() === $mod->getRemoteVersion() ? 'white' : 'green';
            $table->addRow([
                $mod->getSlug(),
                $mod->getFilename(),
                $mod->getLocalVersion(),
                new TableCell(
                    $mod->getRemoteVersion(),
                    [
                        'style' => new TableCellStyle([
                            'fg' => $versionColor,
                        ]),
                    ]),
            ]);
        }
        $table->render();
        return Command::SUCCESS;
    }
}
