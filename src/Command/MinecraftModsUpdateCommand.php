<?php

namespace App\Command;

use App\Client\ModrinthClient;
use App\Repository\ModRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

#[AsCommand(
    name: 'minecraft:mods:update',
    description: 'Add a short description for your command',
)]
class MinecraftModsUpdateCommand extends Command
{


    public function __construct(
        private readonly ModRepository $modRepository,
        private readonly ModrinthClient $modrinthClient,
        private readonly EntityManagerInterface $entityManager,
    ) {
        parent::__construct();
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        $mods = $this->modRepository->findAll();
        foreach ($mods as $mod) {
            $url = null;
            if ($mod->getFilename() === 'Geyser-Fabric.jar') {
                $url = 'https://ci.opencollab.dev/job/GeyserMC/job/Geyser/job/master/lastSuccessfulBuild/artifact/bootstrap/fabric/build/libs/Geyser-Fabric.jar';
            }
            if ($mod->getFilename() === 'floodgate-fabric.jar') {
                $url = 'https://ci.opencollab.dev/job/GeyserMC/job/Floodgate-Fabric/job/master/lastSuccessfulBuild/artifact/build/libs/floodgate-fabric.jar';
            }
            if (!$url) {
                $versions = $this->modrinthClient->getVersions($mod->getSlug());
                if (!isset($versions[0]['version_number'])) {
                    continue;
                }
                if ($versions[0]['version_number'] === $mod->getLocalVersion()) {
                    continue;
                }
                $url = $versions[0]['files'][0]['url'];
                $mod->setRemoteVersion($versions[0]['version_number']);
                $mod->setLocalVersion($versions[0]['version_number']);

            }
            $io->writeln('Downloading <info>' . $url . '</info>');
            $file = file_get_contents($url);
            $filename = basename($url);
            // Delete file
            if (file_exists(__DIR__ . '/mods/' . $mod->getFilename())) {
                $io->writeln('Deleting <info>' . $mod->getFilename() . '</info>');
                unlink(__DIR__ . '/mods/' . $filename);
            }
            file_put_contents('./mods/' . $filename, $file);
            $mod->setFilename($filename);
            $this->entityManager->persist($mod);
        }
        $this->entityManager->flush();
        return Command::SUCCESS;
    }
}
