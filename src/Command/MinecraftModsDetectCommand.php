<?php

namespace App\Command;

use App\Client\ModrinthClient;
use App\Entity\Mod;
use App\Factory\ModFactory;
use App\Repository\DirectoryRepository;
use App\Repository\ModRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

#[AsCommand(
    name: 'minecraft:mods:detect',
    description: 'Add a short description for your command',
)]
class MinecraftModsDetectCommand extends Command
{

    public function __construct(
        private readonly DirectoryRepository $directoryRepository,
        private readonly ModRepository $modRepository,
        private readonly EntityManagerInterface $entityManager,
        private readonly ModrinthClient $modrinthClient,
    ) {
        parent::__construct();
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        $filenames = $this->directoryRepository->getDirectoryContents('mods');
        foreach ($filenames as $filename) {
            if ($this->modRepository->findByFilename($filename)) {
                $io->writeln('Skipping ' . $filename . ' as it is already in db');
                continue;
            }
            $slug = $io->ask('What is the slug of the mod? (or skip)', $filename);
            if ($slug === 'skip') {
                continue;
            }
            $options = [];
            $versions = $this->modrinthClient->getVersions($slug);
            foreach ($versions as $version) {
                $io->writeln(count($options) . ') ' . $version['version_number'] . $version['name']);
                $options[] = $version['version_number'];
            }
            $options[] = Mod::VERSION_IGNORE;
            $selected = $io->ask('What is the version of the mod?', 0);
            if (!isset($options[$selected])) {
                $io->error('Invalid version selected');
                return Command::FAILURE;
            }
            $version = $options[$selected];
            $mod = ModFactory::create(slug: $slug, filename: $filename, localVersion: $version, remoteVersion: $options[0]);
            $this->entityManager->persist($mod);
            $this->entityManager->flush();
        }

//        dd($mods);
        return Command::SUCCESS;
    }
}
