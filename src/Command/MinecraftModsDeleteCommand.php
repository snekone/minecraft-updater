<?php

namespace App\Command;

use App\Repository\ModRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\HttpKernel\KernelInterface;

#[AsCommand(
    name: 'minecraft:mods:delete',
    description: 'Add a short description for your command',
)]
class MinecraftModsDeleteCommand extends Command
{

    public function __construct(
        private readonly ModRepository $modRepository,
        private readonly EntityManagerInterface $entityManager,
        private readonly KernelInterface $appKernel,
    ) {
        parent::__construct();
    }

    protected function configure(): void
    {
        $this->addArgument('slug', InputArgument::REQUIRED, 'Slug of the mod to delete');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        $slug = $input->getArgument('slug');
        $answer = $io->ask('Are you sure you want to delete <info>' . $slug . '</info>?', false);
        if (false === $answer) {
            $io->writeln('Aborting');
            return Command::SUCCESS;
        }
        $io->writeln('Deleting <info>' . $slug . '</info>');
        $mod = $this->modRepository->findBySlug($slug);
        if (!$mod) {
            $io->writeln('Mod <info>' . $slug . '</info> not found');
            return Command::SUCCESS;
        }
        $projectRoot = $this->appKernel->getProjectDir();
        $io->writeln('Deleting <info>' . $projectRoot . '/mods/' . $mod->getFilename() . '</info>');
        unlink($projectRoot . '/mods/' . $mod->getFilename());
        $this->entityManager->remove($mod);
        $this->entityManager->flush();
        $io->writeln('Deleted <info>' . $slug . '</info>');
        return Command::SUCCESS;
    }

}
