<?php

namespace App\Command;

use App\Client\ModrinthClient;
use App\Factory\ModFactory;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\HttpKernel\KernelInterface;

#[AsCommand(
    name: 'minecraft:mods:add',
    description: 'Add a short description for your command',
)]
class MinecraftModsAddCommand extends Command
{

    public function __construct(
        private readonly ModrinthClient $modrinthClient,
        private readonly EntityManagerInterface $entityManager,
        private readonly KernelInterface $appKernel,
        private readonly ModFactory $modFactory,
    ) {
        parent::__construct();
    }

    protected function configure(): void
    {
        $this->addArgument('slug', InputArgument::REQUIRED, 'Slug of the mod to add');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        $slug = $input->getArgument('slug');

        $versions = $this->modrinthClient->getVersions($slug);
        if (!isset($versions[0]['version_number'])) {
            $io->error('Mod with slug <info>' . $slug . '</info> not found');
            return Command::FAILURE;
        }

        $url = $versions[0]['files'][0]['url'];
        $filename = basename($url);
        $mod = $this->modFactory->create(
            slug: $slug,
            filename: $filename,
            localVersion: $versions[0]['version_number'],
            remoteVersion: $versions[0]['version_number']
        );
        $io->writeln('Downloading <info>' . $url . '</info>');
        $file = file_get_contents($url);
        $projectRoot = $this->appKernel->getProjectDir();
        $io->writeln('Adding <info>' . $projectRoot . '/mods/' . $filename . '</info>');
        file_put_contents($projectRoot . '/mods/' . $filename, $file);
        $mod->setFilename($filename);
        $this->entityManager->persist($mod);
        $this->entityManager->flush();

        return Command::SUCCESS;
    }
}
