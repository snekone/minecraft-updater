<?php

namespace App\Command;

use App\Client\ModrinthClient;
use App\Repository\ModRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

#[AsCommand(
    name: 'minecraft:mods:check-updates',
    description: 'Add a short description for your command',
)]
class MinecraftModsCheckUpdatesCommand extends Command
{


    public function __construct(
        private readonly ModRepository $modRepository,
        private readonly ModrinthClient $modrinthClient,
        private readonly EntityManagerInterface $entityManager,
    ) {
        parent::__construct();
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        $mods = $this->modRepository->findAll();
        foreach ($mods as $mod) {
            $versions = $this->modrinthClient->getVersions($mod->getSlug());
            if (!isset($versions[0]['version_number'])) {
                continue;
            }
            if ($versions[0]['version_number'] === $mod->getLocalVersion()) {
                continue;
            }
            $io->writeln('Mod <info>' . $mod->getSlug() . '</info> can be updated to <info>' . $versions[0]['version_number'] . '</info> (<comment>' . ModrinthClient::getModUrl($mod->getSlug()) . '</comment>)');
            $mod->setRemoteVersion($versions[0]['version_number']);
            $this->entityManager->persist($mod);
        }

        return Command::SUCCESS;
    }
}
