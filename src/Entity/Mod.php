<?php

namespace App\Entity;

use App\Repository\ModRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: ModRepository::class)]
#[ORM\Index(columns: ['slug'])]
#[ORM\Index(columns: ['filename'])]
class Mod
{

    public const VERSION_IGNORE = 'ignore';

    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $slug = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $filename = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $localVersion = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $localVersionId = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $remoteVersion = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $remoteVersionId = null;

    public function getLocalVersionId(): ?string
    {
        return $this->localVersionId;
    }

    public function setLocalVersionId(?string $localVersionId): void
    {
        $this->localVersionId = $localVersionId;
    }

    public function getRemoteVersionId(): ?string
    {
        return $this->remoteVersionId;
    }

    public function setRemoteVersionId(?string $remoteVersionId): void
    {
        $this->remoteVersionId = $remoteVersionId;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getSlug(): ?string
    {
        return $this->slug;
    }

    public function setSlug(?string $slug): void
    {
        $this->slug = $slug;
    }

    public function getFilename(): ?string
    {
        return $this->filename;
    }

    public function setFilename(?string $filename): void
    {
        $this->filename = $filename;
    }

    public function getLocalVersion(): ?string
    {
        return $this->localVersion;
    }

    public function setLocalVersion(?string $localVersion): void
    {
        $this->localVersion = $localVersion;
    }

    public function getRemoteVersion(): ?string
    {
        return $this->remoteVersion;
    }

    public function setRemoteVersion(?string $remoteVersion): void
    {
        $this->remoteVersion = $remoteVersion;
    }
}
